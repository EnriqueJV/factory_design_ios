import UIKit


protocol Car{
    var color:String {get set}
    func marca()
}

enum Color{
  case  Rojo
  case Azul
  case Verde
}



class Toyota : Car {
    
    var color: String
    init(ColorToyota colortoyota:String) {
        self.color = colortoyota
    }
    func marca() {
        print("EL carro toyota es de color \(color)")
        
    }
   
}

class Mazda : Car {
    
    var color: String
    init(ColorMazda colormazda:String) {
        self.color = colormazda
    }
    func marca() {
           print("La marca de carro Mazda es de color \(color)")
           
       }
}
class Chevrolet : Car{
    
    var color: String
    init(ColorChevrolet colorchevrolet:String) {
        self.color = colorchevrolet
    }
    func marca() {
           print("La marca de carro Chevrolet es de color \(color)")
           
       }
}

class CarFactory {
    func getCar(Colores colores: Color, Colorc colorc:String)-> Car {
        switch colores {
        case .Azul:
            return Toyota(ColorToyota: colorc)
        case .Rojo:
           return Mazda(ColorMazda: colorc)
        case .Verde:
            return Chevrolet(ColorChevrolet: colorc)
         
        }
       
    }
    
}
    let carro = CarFactory().getCar(Colores:.Azul, Colorc:"Azul")
print( carro.marca())
